var intervalFunction;
var runIsPaused = false;
var round = 1;
var totalElapsed = 0;
var totalTimeAll = 0;
var runOption = { 'workoutVibrate': 0, 'workoutVibrateDuration': 0, 'roundVibrate': 0, 'roundVibrateDuration': 0, 'workoutLeadTime': 0, 'showQuote': 0, 'showQuoteInterval': 5 };
var runID;
var quoteChecked = false;
var quoteTotal = 0;

func['run'] = function (data) {
    let id = data.id;
    runID = id;
    initVars();

    setRunOptions(a => {
        initRunTimer(id);
        if (!runOption.showQuote) document.getElementById("runQuote").classList.add("d-none");
        else document.getElementById("runQuote").classList.remove("d-none");
    });

}

function initRunTimer(id) {
    singleTransaction("SELECT * FROM workout WHERE planID=?", [id], function (rows) {
        console.log(rows);
        if (rows.length == 0) {
            toastr.error("An error Occured when trying to open this workout");
            changePage('workouts', null, false);
        } else {
            var countDown = document.getElementById("countDown");
            var workouts = rows;
            var interval = workouts[0].interval;

            //getTotal time
            var i = 0;
            for (var innerRow of rows) {
                totalTimeAll += innerRow.interval + (i < rows.length - 1 ? runOption.workoutLeadTime : 0);
                i++;
            }

            var i = 0;
            var workoutIndex = 0;
            if (workouts[workoutIndex].name.toLowerCase() == "rest") document.getElementById("runDetails").classList.add("restRun");
            else document.getElementById("runDetails").classList.remove("restRun");
            document.getElementById("workoutName").innerHTML = workouts[workoutIndex].name;
            if (workouts.length == 1) document.getElementById("workoutNameNext").innerHTML = workouts[workoutIndex].name;
            else document.getElementById("workoutNameNext").innerHTML = workouts[workoutIndex + 1].name;
            document.getElementById("round").innerHTML = round;

            intervalFunction = setInterval(function () {
                //console.log(i, interval - i, (interval - i) + "s");
                if (runOption.showQuote && totalElapsed % runOption.showQuoteInterval == 0) {
                    getRunQuote();
                }
                if (!runIsPaused) {
                    if (i < runOption.workoutLeadTime) {
                        document.getElementById("runDetails").classList.add("leadTime");
                    } else {
                        document.getElementById("runDetails").classList.remove("leadTime");
                    }
                    if (i > interval + runOption.workoutLeadTime) {
                        i = 0;
                        workoutIndex++;
                        if (workoutIndex >= workouts.length) {
                            workoutIndex = 0;
                            navigator.notification.beep(1);
                            if (runOption.roundVibrate == "1") navigator.notification.vibrate(parseInt(runOption.roundVibrateDuration) * 1000);

                            round++;
                            document.getElementById("round").innerHTML = round;
                        } else {
                            navigator.notification.beep(1);
                            if (runOption.workoutVibrate == "1") navigator.notification.vibrate(parseInt(runOption.workoutVibrateDuration) * 1000);
                        }
                        document.getElementById("workoutName").innerHTML = workouts[workoutIndex].name;
                        if (workouts[workoutIndex].name.toLowerCase() == "rest") document.getElementById("runDetails").classList.add("restRun");
                        else document.getElementById("runDetails").classList.remove("restRun");
                        if (workoutIndex + 1 > workouts.length) document.getElementById("workoutNameNext").innerHTML = workouts[0].name;
                        else document.getElementById("workoutNameNext").innerHTML = workouts[workoutIndex + 1].name;
                        interval = workouts[workoutIndex].interval + runOption.workoutLeadTime;
                    }

                    countDown.innerHTML = ((i < runOption.workoutLeadTime ? runOption.workoutLeadTime : interval + runOption.workoutLeadTime) - i) + "s";
                    document.getElementById("totalTimeElapsed").innerHTML = durationTextify(totalElapsed, true);
                    document.getElementById("totalTimeRemaining").innerHTML = durationTextify(totalTimeAll - (totalElapsed - ((round - 1) * totalTimeAll)), true);
                    i++;
                    totalElapsed++;
                }
            }, 1000);
        }
    });
}

function setRunOptions(callback, i = 0) {
    if (i < Object.keys(runOption).length) {
        var option = Object.keys(runOption)[i];
        getOption(option, value => {
            console.log(option, value);
            if (!isNaN(parseFloat(value))) value = parseFloat(value);
            runOption[option] = value;
            i++;
            setRunOptions(callback, i);
        });
    } else {
        callback();
    }

}

function getRunQuote() {
    if (!quoteChecked) {
        singleTransaction("SELECT COUNT(quote) as total FROM quote", [], rows => {
            console.log("TOTAL", rows, rows[0]['total'] > 0);
            if (rows[0]['total'] <= 0) {
                runOption.showQuote = 0;
            } else {
                quoteTotal = rows[0]['total'];
                fetchRandomQuote();
            }
        })
    } else {
        if (quoteTotal > 0) fetchRandomQuote();
    }
}

function fetchRandomQuote() {
    singleTransaction("SELECT author, quote FROM quote WHERE rowid=?", [generateNumber(quoteTotal)], row => {
        document.getElementById("quoteBody").innerHTML = row[0].quote;
        document.getElementById("quotePerson").innerHTML = "~" + row[0].author;
    });
}

function initVars() {
    runIsPaused = false;
    round = 1;
    totalElapsed = 0;
    totalTimeAll = 0;
    quoteChecked = false;
    quoteTotal = 0;
}

function runBack() {
    window.clearInterval(intervalFunction);
    changePage('workouts', null, false)
}

function runRestart() {
    window.clearInterval(intervalFunction);
    initRunTimer(runID);
}

function runPause(e) {
    var elem = document.getElementById("runPause");
    elem.classList.toggle("fa-pause");
    elem.classList.toggle("fa-play");

    runIsPaused = !runIsPaused;
}