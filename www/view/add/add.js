var workouts = [];
var oldDuration = 0;
var editAdd = false;
var addPlanID = null;

func['add'] = function (data = null) {
    if (Object.keys(data).length > 0) {
        editAdd = true;
        addPlanID = data.id;
        displayWorkoutPlan(data.id);
    } else {
        editAdd = false;
    }
    workouts = [];
    oldDuration = 0;
}

function displayWorkoutPlan(id) {
    singleTransaction("SELECT name FROM plan WHERE id=?", [id], function (rows) {
        document.getElementById("planName").value = rows[0]['name'];
        singleTransaction("SELECT name, interval FROM workout WHERE planID=?", [id], function (rows) {
            for (let row of rows) {
                createWorkout(row.name.toLowerCase() == "rest", row.name, row.interval);
            }
        });
    });
}

function createWorkout(rest = false, name = null, duration = null) {
    if (name == null) name = document.getElementById("workoutInputName").value;
    if (duration == null) duration = parseInt(document.getElementById("workoutInputDuration").value);
    if (name != "" && !isNaN(duration) && duration > 5) {
        oldDuration = duration;
        var id = generate(15);
        workouts.push({ name: name, duration: duration });
        document.getElementById("createdWorkout").insertBefore(
            createElement("div", { class: "section p-0 workoutsList w-100 animated fadeIn workoutBox flex black position-relative" + (rest ? " rest" : ""), id: id }, [
                createElement("div", { class: "d-flex position-relative" }, [
                    createElement("div", { class: "position-relative w-100 p-3" }, [
                        createElement("div", { class: "workoutElem lg word-break dropdownInput name" }, [], { innerHTML: name }),
                        createElement("div", { class: "d-flex" }, [
                            createElement("div", { class: "position-relative p-0 workoutDurationLabel dropdownInput duration", type: "number", min: "1", max: "1000" }, [], { innerHTML: duration }),
                            createElement("div", { class: "workoutDurationLabel p-0" }, [], { innerHTML: "s" })
                        ])

                    ]),
                    createElement("div", { class: "position-relative w-25 d-flex flex-column" }, [
                        createElement("div", { class: "p-1 button workoutRemoveButton  fa fa-times position-relative m-0 square", onclick: "removeAddWorkout('" + id + "', " + (workouts.length - 1) + ")" }),
                        createElement("div", { class: "p-1 button workoutRemoveButton  fa fa-pen position-relative m-0 square", onclick: "editAddWorkout('" + id + "', " + (workouts.length - 1) + ")" }),

                    ])
                ])

            ]),
            document.getElementById("createdWorkout").children[0]
        );
        document.getElementById("workoutInputName").value = "";
        document.getElementById("workoutInputName").focus();
    } else {
        toastr.warning("Invalid Information. Make sure your duration is more than 5s");
    }
}

function removeAddWorkout(id, index) {
    removeIndirectElement(id);
    workouts.splice(index, 1);
}

function editAddWorkout(id, index) {
    var target = window.event.target;
    if (target.classList.contains("fa-pen")) {// now editing
        $("#" + id).find(".dropdownInput").each((i, elem) => {
            replaceElem(elem, "input", 1, true);
        });
    } else {// restoring to previous -- to save changes
        var name, duration, elems, valid = true;
        elems = $("#" + id).find(".dropdownInput");
        elems.each((i, elem) => {
            if (elem.classList.contains("name")) name = elem.value;
            if (elem.classList.contains("duration")) duration = parseInt(elem.value);
        });
        if (name != "" && !isNaN(duration) && duration > 15) {
            elems.each((i, elem) => { replaceElem(elem, "div", 2, true) });
            workouts[index] = { name: name, duration: duration };
            if (name.toLowerCase() == "rest") $("#" + id)[0].classList.add("rest");
            else $("#" + id)[0].classList.remove("rest");
        } else {
            toastr.warning("Make sure your duration is more than 15s", "Invalid Information");
            return;
        }
    }
    target.classList.toggle("fa-pen");
    target.classList.toggle("fa-check");
}

function createRest() {
    var d = oldDuration;
    document.getElementById("workoutInputName").value = "REST";
    createWorkout(true);
    document.getElementById("workoutInputName").value = "";
    document.getElementById("workoutInputDuration").value = d;
    document.getElementById("workoutInputName").focus();
}

function workoutAddNamekeyUp(e){
    if (e.keyCode == 13) {
        document.getElementById("workoutInputDuration").focus();
    }
}

function workoutAddkeyUp(e) {
    if (e.keyCode == 13) {
        createWorkout();
    }
}
function workoutPlankeyUp(e) {
    if (e.keyCode == 13) {
        document.getElementById("workoutInputName").focus();
    }
}

function saveWorkout() {


    var name = document.getElementById("planName").value;

    if (name == "") {
        toastr.warning("Please provide a Plan Name");
    } else {

        if (workouts.length == 0) {
            toastr.warning("Please provide at least one workout for the plan");
            return;
        }

        singleTransaction(editAdd ? "DELETE FROM plan WHERE id=?" : "SELECT ?", [addPlanID], rows => {
            singleTransaction(editAdd ? "DELETE FROM workout WHERE planID=?" : "SELECT ?", [addPlanID], rows => {
                var id = NewGuid();
                singleTransaction("INSERT INTO plan (id, name) VALUES (?, ?);", [id, name], function (rows, tx, rx) {
                    saveIndividualWorkout(id, function () {
                        toastr.success("Workout Plan Saved!");
                        changePage("workouts", null, false);
                    });
                });
            });
        });


    }
}

function saveIndividualWorkout(id, callback = null, i = 0) {
    if (i < workouts.length) {
        var w = workouts[i];
        singleTransaction("INSERT INTO workout (id, name, interval, planID) VALUES (?, ?, ?, ?);", [NewGuid(), w.name, w.duration, id], function (rows, tx, rx) {
            i++;
            saveIndividualWorkout(id, callback, i);
        });
    } else {
        if (typeof callback == "function") callback();
    }


}


