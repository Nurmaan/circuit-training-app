loginEmail = "";
loginCallback = null;
loginSkipCallback = null;
func['login'] = function () {

}

function login() {
    var res = result(document.getElementById("loginForm"));
    if (res) {
        if (true /*document.getElementById("loginPrivacyPolicyCheck").checked*/) {
            res['accountType'] = 1;
            res['requestType'] = "login";
            loginEmail = res['email']
            new Http("login", function (data) {
                if (typeof data != "object" && data.toLowerCase() == "reset password") {
                    showChangePassword();
                } else {
                    toastr.success("Logged In");
                    if (typeof loginCallback == "function") loginCallback();
                    else changePage("workouts");
                }
            }).post(res);
        } else {
            toastr.warning("Please agree to the <a href='http://scootit.nurmaan.mauriapp.info/privacyPolicy' target='_blank' class='ml-2 mr-2'>Privacy Policy</a> before you can proceed further");
        }

    }

}

function skipLogin() {
    if (typeof loginSkipCallback == "function") loginSkipCallback();
    else changePage('workouts', null, false);
}

function register() {
    var res = result(document.getElementById("registerForm"));
    if (res) {
        if (true) {
            res['accountType'] = 1;
            res['requestType'] = "signup";
            loginEmail = res['email'];
            new Http("login", function () {
                toastr.info("Please verify your email");
                showValidateEmail();
            }).post(res);
        } else {
            toastr.warning("Please agree to the <a href='http://scootit.nurmaan.mauriapp.info/privacyPolicy' target='_blank' class='ml-2 mr-2'>Privacy Policy</a> before you can proceed further");
        }

    }
}

function forgotPassword() {
    var res = result(document.getElementById("forgotPasswordForm"));
    res['requestType'] = 'forgotPassword';
    new Http().post('login', res, function () {
        setTimeout(function () {
            toastr.info("Check your email for further instructions");
        }, 5000);
    })
}

function resendValidation() {
    if (loginEmail != "") {
        new Http().post("login", { requestType: "resendValidation", email: loginEmail }, function () {
            setTimeout(function () {
                toastr.info("Email Sent");
            }, 5000);
        });
    } else {
        toastr.warning("Could not obtain your email address. Please try to login/register first");
    }
}

function changePassword() {
    var res = result(document.getElementById("changePasswordForm"));
    res['requestType'] = 'changePassword';
    res['email'] = loginEmail;
    new Http().post('login', res, function () {
        toastr.success("Your password has been changed! Please Login");
        showSignIn();
    });
}

function showSignUp() {
    document.getElementById("loginForm").classList.add("d-none");
    document.getElementById("registerForm").classList.remove("d-none");
    document.getElementById("forgotPasswordForm").classList.add("d-none");
    document.getElementById("validateEmailForm").classList.add("d-none");
    document.getElementById("changePasswordForm").classList.add("d-none");
}
function showSignIn() {
    document.getElementById("loginForm").classList.remove("d-none");
    document.getElementById("registerForm").classList.add("d-none");
    document.getElementById("forgotPasswordForm").classList.add("d-none");
    document.getElementById("validateEmailForm").classList.add("d-none");
    document.getElementById("changePasswordForm").classList.add("d-none");
}
function showValidateEmail() {
    document.getElementById("validateEmailForm").classList.remove("d-none");
    document.getElementById("registerForm").classList.add("d-none");
    document.getElementById("forgotPasswordForm").classList.add("d-none");
    document.getElementById("loginForm").classList.add("d-none");
    document.getElementById("changePasswordForm").classList.add("d-none");
}
function showForgotPassword() {
    document.getElementById("loginForm").classList.add("d-none");
    document.getElementById("registerForm").classList.add("d-none");
    document.getElementById("forgotPasswordForm").classList.remove("d-none");
    document.getElementById("validateEmailForm").classList.add("d-none");
    document.getElementById("changePasswordForm").classList.add("d-none");
}
function showChangePassword() {
    document.getElementById("loginForm").classList.add("d-none");
    document.getElementById("registerForm").classList.add("d-none");
    document.getElementById("changePasswordForm").classList.remove("d-none");
    document.getElementById("validateEmailForm").classList.add("d-none");
    document.getElementById("forgotPasswordForm").classList.add("d-none");
}