func['settings'] = function () {
    setTimeout(initLoginDetails, 100);
    getOption("workoutVibrate", value => {
        if (value == "1") {
            document.getElementById("workoutVibrateInput").setAttribute("checked", true);
            document.getElementById("workoutVibrateSlider").classList.remove("d-none");
        } else {
            document.getElementById("workoutVibrateSlider").classList.add("d-none");
        }
    });
    getOption("roundVibrate", value => {
        if (value == "1") {
            document.getElementById("roundVibrateInput").setAttribute("checked", true);
            document.getElementById("roundVibrateSlider").classList.remove("d-none");
        } else {
            document.getElementById("roundVibrateSlider").classList.add("d-none");
        }
    });
    getOption("showQuote", value => {
        if (value == "1") {
            document.getElementById("showQuoteInput").setAttribute("checked", true);
            document.getElementById("showQuoteIntervalSlider").classList.remove("d-none");
        } else {
            document.getElementById("showQuoteIntervalSlider").classList.add("d-none");
        }
    });
    getOption("workoutVibrateDuration", value => {
        document.getElementById("workoutVibrateDurationInput").value = value;
        document.getElementById("workoutVibrateDurationValue").innerHTML = value + "s";
    });
    getOption("roundVibrateDuration", value => {
        document.getElementById("roundVibrateDurationInput").value = value;
        document.getElementById("roundVibrateDurationValue").innerHTML = value + "s";
    });
    getOption("showQuoteInterval", value => {
        document.getElementById("showQuoteIntervalInput").value = value;
        document.getElementById("showQuoteIntervalValue").innerHTML = durationTextify(parseInt(value), true);
    });
    getOption("workoutLeadTime", value => {
        document.getElementById("workoutLeadDurationInput").value = value;
        document.getElementById("workoutLeadDurationValue").innerHTML = value + "s";
    });
}

function initLoginDetails() {
    var username = localStorage.getItem("username");
    var target = document.getElementById("settingsLoginButtonContainer");
    target.innerHTML = "";
    console.log("username", username);
    if (username != "" && username != null) {
        target.appendChild(createElement("div", { class: "text-center" }, [
            createElement("div", { class: "text-center" }, [], { innerHTML: username }),
            createElement("div", { class: "text-center button square dark", onclick: "logout()" }, [], { innerHTML: "Logout" })
        ]));
    } else {
        $(".loginRequiredSettingsButton").addClass("disabled");
        target.appendChild(createElement("div", { class: "button dark router pt-2 pb-2", link: "login" }, [], { innerHTML: "Login/Register" }));
        router();
        loginSkipCallback = function () {
            changePage("settings", null, false);
        }
        loginCallback = function () {
            changePage("settings", null, true);
        }
        $(".button.disabled").each((i, elem) => {
            elem.onclick = function () { };
        });
    }
}

function backup() {
    let data = {};
    singleTransaction("SELECT name FROM SQLITE_MASTER WHERE type='table'", [], function (rows) {
        backupTables(rows, data);
    });
}

function backupTables(rows, data, i = 0) {
    if (i < rows.length) {
        let row = rows[i];
        if (row.name.includes("_") || row.name.toLowerCase() == "option" || row.name.toLowerCase() == "quote") {
            i++;
            backupTables(rows, data, i);
        } else {
            singleTransaction("SELECT * FROM " + row.name, [], function (innerRows) {
                data[row.name] = [];
                for (var innerRow of innerRows) data[row.name].push(innerRow);
                i++;
                backupTables(rows, data, i);
            });
        }

    } else {
        new Http("backup", function () {
            toastr.success("Data has been backed up");
        }).post({ data: JSON.stringify(data).split('"').join("'"), requestType: "backup" });
    }
}

function restore() {
    var id = generate(30);
    var elem = createElement("div", {}, [
        createElement("div", { class: "d-flex" }, [
            createElement("span", { class: "checkbox checked mr-2", id: id }),
            createElement("span", {}, [], { innerHTML: "Preserve Existing Data" })
        ])
    ])
    confirm("Restore Data", elem.innerHTML, function () {
        toastr.warning("Please do not close the application", "Restoring...");
        let preserve = document.getElementById(id).classList.contains("checked");
        new Http('backup', function (dat) {
            console.log(dat);
            if (dat != null) {
                var data = JSON.parse(dat.split("'").join('"'));
                console.log("restore data", dat);
                let completed = 0;
                for (let table in data) {
                    singleTransaction(preserve ? "SELECT 1 + 1" : "DELETE FROM " + table, [], function (rows) {
                        restoreTable(table, data[table], a => {
                            completed++;
                            if (completed == Object.keys(data).length - 1) {
                                toastr.success("All your Workout plans have been restored", "Restore Complete");
                            }
                        });
                    });
                }
            } else {
                toastr.info("Please consider backing up your data first", "No Restore Data");
            }

        }).post({ requestType: "restore" });
    }, null, ["Restore", "Cancel"], function () {
        $("#" + id).click(function () {
            this.classList.toggle("checked");
        })
    });

}

function restoreTable(table, rows, callback = null, i = 0) {
    console.log("Restoring " + table, rows, i);
    if (i < rows.length) {
        var sql = "INSERT INTO " + table + " (";
        var values = " VALUES (";
        var len = Object.keys(rows[i]).length;
        var j = 0;
        for (var key in rows[i]) {
            sql += key + (j < len - 1 ? "," : ")");
            values += (typeof rows[i][key] == "string" ? "'" + rows[i][key] + "'" : "" + rows[i][key] + "") + (j < len - 1 ? "," : ")");
            j++;
        }

        singleTransaction("SELECT * FROM " + table + " WHERE id=?", [rows[i]['id']], function (innerRows) {
            console.log("Does not already exists", innerRows.length == 0);
            if (innerRows.length == 0) {
                singleTransaction(sql + values, [], function () {
                    i++;
                    restoreTable(table, rows, callback, i);
                });
            } else {
                i++;
                restoreTable(table, rows, callback, i);
            }
        });

    } else {
        if (typeof callback == "function") callback();
    }
}

function workoutVibrate(e) {
    if (e.target.checked) {
        setOption("workoutVibrate", "1");
        document.getElementById("workoutVibrateSlider").classList.remove("d-none");
    } else {
        setOption("workoutVibrate", "0");
        document.getElementById("workoutVibrateSlider").classList.add("d-none");
    }
}
function showQuote(e) {
    if (e.target.checked) {
        setOption("showQuote", "1");
        document.getElementById("showQuoteIntervalSlider").classList.remove("d-none");
    } else {
        setOption("showQuote", "0");
        document.getElementById("showQuoteIntervalSlider").classList.add("d-none");
    }
}

function roundVibrate(e) {
    if (e.target.checked) {
        setOption("roundVibrate", "1");
        document.getElementById("roundVibrateSlider").classList.remove("d-none");
    } else {
        setOption("roundVibrate", "0");
        document.getElementById("roundVibrateSlider").classList.add("d-none");
    }
}

function workoutVibrateDurationInput(e) {
    setOption("workoutVibrateDuration", e.target.value, false, function () {
        document.getElementById("workoutVibrateDurationValue").innerHTML = e.target.value + "s";
    });
}
function showQuoteIntervalInput(e) {
    setOption("showQuoteInterval", e.target.value, false, function () {
        document.getElementById("showQuoteIntervalValue").innerHTML = durationTextify(parseInt(e.target.value), true);
    });
}
function roundVibrateDurationInput(e) {
    setOption("roundVibrateDuration", e.target.value, false, function () {
        document.getElementById("roundVibrateDurationValue").innerHTML = e.target.value + "s";
    });
}
function workoutLeadDuration(e) {
    setOption("workoutLeadTime", e.target.value, false, function () {
        document.getElementById("workoutLeadDurationValue").innerHTML = e.target.value + "s";
    });
}
