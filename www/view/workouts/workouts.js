var workoutsAnimations = ["fadeInRight", "fadeInLeft", "fadeInDown"];

func['workouts'] = function () {
    singleTransaction("SELECT * FROM plan", [], function (rows) {
        displayWorkoutsElement(rows);
    });
}

function displayWorkoutsElement(rows, i = 0) {
    if (i < rows.length) {
        let row = rows[i];
        var animation = workoutsAnimations[generateNumber(workoutsAnimations.length - 1)];
        singleTransaction("SELECT * FROM workout WHERE planID = ?", [row.id], function (innerRows) {
            var duration = 0;
            for (var innerRow of innerRows) duration += innerRow.interval;
            document.getElementById("workoutsBody").appendChild(
                createElement("div", { class: "section p-0 animated faster " + animation }, [
                    createElement("div", { class: "workoutsMainBody", "data-id": row.id }, [
                        createElement("div", { class: "d-flex" }, [
                            createElement("div", { class: "p-2 pb-0 word-break workoutsMainText w-100", "data-id": row.id }, [], { innerHTML: row.name }),
                            createElement("div", { class: "fa fa-info-circle w-fit p-2 infoIcon" })
                        ]),
                        createElement("div", { class: "d-flex pt-0 w-100 p-2 mb-2" }, [
                            createElement("div", { class: "workoutsMainWorkouts mr-1" }, [], { innerHTML: innerRows.length + " Workout" + (innerRows.length > 1 ? "s" : "") }),
                            createElement("div", { class: "workoutsMainDuration" }, [], { innerHTML: durationTextify(duration) })
                        ])
                    ]),
                    createElement("div", { class: "d-flex" }, [
                        createElement("div", { class: "m-0 button square secondary p-2 w-100 fa fa-play", onclick: "changePage('run',{id:'" + row.id + "'})" }),
                        createElement("div", { class: "m-0 button square secondary p-2 w-100 fa fa-pen", onclick: "changePage('add',{id:'" + row.id + "'})" }),
                        createElement("div", { class: "m-0 button dark square secondary p-2 w-100 fa fa-trash", onclick: "removeWorkout('" + row.id + "')" })
                    ])
                ])
            );

            $(".workoutsMainBody").unbind().click(e => { getParentByClassName(e.target, "workoutsMainBody").classList.toggle("active") });
        });
        i++;
        setTimeout(function () {
            displayWorkoutsElement(rows, i);
        }, generateNumber(340, 100));

    }
}

function durationTextify(seconds, compact = false) {
    var hours = seconds / 3600;
    var minutes = (hours - Math.floor(hours)) * 60;
    var seconds = (minutes - Math.floor(minutes)) * 60;

    hours = Math.floor(hours);
    minutes = Math.floor(minutes);
    seconds = Math.floor(seconds);

    var ret = "";
    if (hours > 0) {
        if (!compact) ret = hours + " hour" + (hours > 1 ? "s " : " ") + minutes + " minute" + (minutes > 1 ? "s " : " ") + seconds + " second" + (seconds > 1 ? "s " : "");
        else ret = hours + "hr" + (hours > 1 ? "s " : " ") + minutes + "min" + (minutes > 1 ? "s " : " ") + seconds + "s";
    } else if (minutes > 0) {
        if (!compact) ret = minutes + " minute" + (minutes > 1 ? "s " : " ") + seconds + " second" + (seconds > 1 ? "s " : "");
        else ret = minutes + "min" + (minutes > 1 ? "s " : " ") + seconds + "s";
    } else {
        if (!compact) ret = seconds + " second" + (seconds > 1 ? "s " : "");
        else ret = seconds + "s";
    }

    return ret;
}

function removeWorkout(id) {
    confirm("Remove Workout", "Are you Sure you want to Remove this workout?", a => {
        singleTransaction("DELETE FROM plan WHERE id=?", [id], function () {
            singleTransaction("DELETE FROM workout WHERE planID=?", [id], function () {
                toastr.success("Workout Plan has been Removed");
                changePage('workouts', null, true, true);
            });
        });
    }, null, ['Yes', "No"]);

}