import os

#adding js files from www/js
js = ""
for root, dirs, files in os.walk("./js"):
    for filename in files:
        js += "\t<script type='text/javascript' src='js/" + filename + "'></script>\n"

#adding css files from www/css
css = ""
for root, dirs, files in os.walk("./css"):
    for filename in files:
        css += "\t<link rel='stylesheet' type='text/css' href='css/" + filename + "' />\n"

#getting the current HTML of index.html
f = open("./index.html", "r")
innerHTML = f.read()
f.close()

#adding the css and js files of the views
for root, dirs, files in os.walk("./view"):
    for directory in dirs:
        css += "\t<link rel='stylesheet' type='text/css' href='view/" + directory + "/" + directory + ".css' />\n"
        js += "\t<script type='text/javascript' src='view/" + directory + "/" + directory + ".js'></script>\n"

#clearing the existing css and js declarations in index.html
startHTML = innerHTML[:(innerHTML.index("<!-- css start -->") + 18)] + "\n--css--"
bodyHTML = "\t" + innerHTML[innerHTML.index("<!-- css end -->"):]
jsHTML = bodyHTML[:(bodyHTML.index("<!-- js start -->") + 17)] + "\n--js--"
endHTML = "\t" + bodyHTML[bodyHTML.index("<!-- js end -->"):]
innerHTML = startHTML + jsHTML + endHTML

#setting the css and js
innerHTML = innerHTML.replace("--js--", js)
innerHTML = innerHTML.replace("--css--", css)

f = open("./index.html", "w+")
f.write(innerHTML)
f.close()

