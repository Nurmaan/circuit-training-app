import os


def main():
    view = input("Name of new View")
    path = "./view/" + view
    if not os.path.exists(path):
        os.makedirs(path)
    
    f = open("./templates/html.html", "r")
    innerHTML = f.read()
    f.close()

    innerHTML = innerHTML.replace("--name--", view)

    f = open(path + "/" + view + ".html", "w+")
    f.write(innerHTML)
    f.close()

    f = open(path + "/" + view + ".css", "w+")
    f.close()

    f = open(path + "/" + view + ".js", "w+")
    f.close()

    f = open("./index.html", "r")
    index = f.read()
    f.close()
    index = index.replace("<!-- js end -->", "<script type='text/javascript' src='view/" + view + "/" + view + ".js'></script>\n\t<!-- js end -->")
    index = index.replace("<!-- css end -->", "<link rel='stylesheet' type='text/css' href='view/" + view + "/" + view + ".css'/>\n\t<!-- css end -->")
    f = open("./index.html", "w")
    f.write(index)
    f.close()

if __name__ == "__main__":
    main()
