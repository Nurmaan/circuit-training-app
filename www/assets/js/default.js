
func = {};
validityRadio = {};
baseUrl = "";
currentPage = "";
var historyData = [];
var lastSessionCheck = 0;

prod = false;

function init() {// call initial functions
    console.log("init default");
    initialiseTable(function () {
        getQuote();
        if ((localStorage.getItem("login") == null || localStorage.getItem("login") == "true")) {
            changePage("login");
        } else {
            changePage("workouts");
        }
    });
    //checkScroll();
    //checkFailedRequests();
    loginSkipCallback = function () {
        localStorage.setItem("login", false);
        changePage("workouts", null, false);
    }
    loginCallback = function () {
        localStorage.setItem("login", false);
        changePage("workouts");
    }
    //checkStatusbar();
}

function checkStatusbar() {
    if (cordova.platformId == 'android') {
        StatusBar.overlaysWebView(true);
        StatusBar.backgroundColorByHexString('#33000000');
    }
}

function getQuote() {
    singleTransaction("SELECT COUNT(id) as total FROM quote", [], rows => {
        if (rows[0]['total'] == 0) {
            if (window.navigator.onLine) {
                $.get("https://type.fit/api/quotes", d => {
                    var data = JSON.parse(d);
                    data.push({ author: "total", text: data.length.toString() });
                    insertQuotesToDB(data);
                });
            }
        }
    });
}

function insertQuotesToDB(quotes, callback = null, i = 0) {
    if (i < quotes.length) {
        singleTransaction("INSERT INTO quote (id,author,quote) VALUES(?,?,?)", [NewGuid(), quotes[i].author == null ? "" : quotes[i].author, quotes[i].text], a => {
            i++;
            insertQuotesToDB(quotes, callback, i);
        });
    } else {
        if (typeof callback == "function") callback();
    }
}

function checkSession(callback, force = false) {
    if (force || Date.now() - lastSessionCheck >= 1000 * 60 * 5) {
        new Http("sessionTest", function () {
            if (typeof callback == "function") callback();
            lastSessionCheck = Date.now();
        }).post({});
    } else {
        if (typeof callback == "function") callback();
    }
}

function logout() {
    window.event.stopPropagation();
    //hideSideMenu();
    localStorage.removeItem('userToken');
    localStorage.removeItem('username');
    localStorage.removeItem('userType');
    changePage("login", null, false);
    lastSessionCheck = 0;
}

function confirm(title = "Circuit Training", message, success, error, buttons = ["Confirm", "Cancel"], drawComplete = null) {
    ($('#confirm')).modal({ backdrop: 'static', keyboard: false });
    $('#confirm')[0].style.zIndex = "10001";

    //send to foreground
    var modals = document.getElementsByClassName("modal-backdrop fade show");
    (modals[modals.length - 1]).style.zIndex = "10000";

    $('#confirmAccept')[0].innerHTML = buttons[0];
    if (buttons.length > 1) {
        $('#confirmClose')[0].innerHTML = buttons[1];
        $('#confirmClose').removeClass("d-none");
    } else {
        $('#confirmClose').addClass("d-none");
    }
    $('#confirmBody')[0].innerHTML = message;
    $('#confirmTitle')[0].innerHTML = title;

    if (typeof drawComplete == "function") drawComplete();

    $('#confirmClose').unbind().click(function () {
        if (typeof error == "function") if (error()) ($("#confirm")).modal("hide");
    });
    $('#confirmAccept').unbind().click(function () {
        if (typeof success == "function") if (success()) ($("#confirm")).modal("hide");
    });

    $(".closeModal").click(e => {
        if (this.modalClosable) ($("#confirm")).modal("hide");
    });
}

/* --------------------------------- INPUT VALIDATION ------------------------------*/
function validate(parent = null) {
    if (parent == null) parent = document.body;
    var elems = $(parent).find("input, textarea");
    var ret = true;
    var password = "";
    clearValidateError();

    for (var i = 0; i < elems.length; i++) {
        var elem = elems[i];
        var validity;
        if (elem.getAttribute("required") != null && (!(validity = isInputValid(elem)).result || (elem.value == "" && elem.innerHTML == ""))) {

            ret = false;
            displayElementError(parent, elem, validity['msg']);
        }

        if (elem.getAttribute("password") != null) {
            password = elem.value || elem.innerHTML;
        }
        if (elem.getAttribute("password-repeat") != null && password != (elem.value || elem.innerHTML)) {
            displayElementError(parent, elem, "Passwords do not match");
            ret = false;
        }
    }
    return ret;
}

function displayElementError(parent, elem, error = "") {
    $(parent).animate({ scrollTop: $(elem).offset().top }, 100);
    if (elem.getAttribute("error") != null) error = elem.getAttribute("error");
    var id = generate(15);
    var newNode = createElement("div", { class: "validateError position-relative", id: id }, [], { innerHTML: error });
    insertAfter(newNode, elem);
    elem.style.marginBottom = "1em";
    elem.style.border = "1px solid red!important";
}

function result(parent = document.body) {
    if (validate(parent)) {
        var res = {};
        var chosen = [];
        var collections = $(parent).find(".collection");
        var groups = $(parent).find(".group");

        if (collections.length > 0) {
            for (var collection of collections) {
                var innerResult = [];
                $(collection).find(".group").each((k, elem) => {
                    var inner2 = {}
                    $(elem).find("input,textarea").each((j, innerElem) => {
                        inner2[innerElem.getAttribute("result")] = innerElem.value || innerElem.innerHTML;
                        chosen.push(innerElem);
                    });
                    innerResult.push(inner2);
                });
                res[collection.getAttribute("result")] = innerResult;
            }
        } else if (groups.length > 0) {
            for (var group of groups) {
                var name = group.getAttribute('result');
                res[name] = {};
                $(group).find("input,textarea").each((i, elem) => {
                    res[name][elem.getAttribute('result')] = elem.innerHTML || elem.value;
                    chosen.push(elem);
                })
            }
        }

        $(parent).find("input,textarea,select").each((i, elem) => {
            if (elem.getAttribute("result") != null && chosen.indexOf(elem) == -1) {
                var type = elem.type;
                if (type == "number" || type == "text" || type == "phone" || type == "email" || type == "textarea" || type == "password" || type == "date" || type == "tel") {
                    res[elem.getAttribute("result")] = elem.value || elem.innerHTML;
                } else if (type == "radio") {
                    if (elem.checked) res[elem.getAttribute("result")] = elem.getAttribute("value");
                } else if (type == "select-one") {
                    res[elem.getAttribute("result")] = elem.options[elem.selectedIndex].text;
                }
            }

        });
        return res;
    } else {
        return false;
    }
}

function isInputValid(input) {
    var type = input.type;
    var value = input.innerHTML || input.value;
    var ret = true;
    var msg = "This Field is Required";
    switch (type) {
        case "email":
            if (!value.includes("@") || !value.includes(".")) {
                ret = false
                msg = "Invalid Email Format"
            }
            break;
        case "number":
            if (isNaN(parseInt(value))) {
                ret = false;
                msg = "Invalid Number";
            }
            break;
        case "phone":
            if (isNaN(parseInt(value)) || value > 30 || value < 4) {
                ret = false;
                msg = "Invalid Phone Number | Do not add brackets";
            }
            break;
        case "radio":
            if (typeof validityRadio[input.name]) {
                validityRadio[input.name] = false;
            }
            if (input.checked) validityRadio[input.name] = true;
    }

    if (input.getAttribute("min-length") != null && value.length < input.getAttribute("min-length")) {
        ret = false;
        msg += " Value is too Short (> " + input.getAttribute("min-length") + ")";
    }
    if (input.getAttribute("max-length") != null && value.length > input.getAttribute("max-length")) {
        ret = false;
        msg += " Value is too Long (< " + input.getAttribute("max-length") + ")";
    }
    return { result: ret, msg: msg };
}

function clearValidateError() {
    var errorElems = document.getElementsByClassName("validateError");
    while (errorElems.length > 0) {
        var errorElem = errorElems[0];
        errorElem.previousSibling.style.marginBottom = "auto";
        removeElement(errorElem);
    }
}

function insertAfter(newNode, referenceNode) {
    referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
}
/* --------------------------------- END INPUT VALIDATION ------------------------------*/
/* -------------------- Helpers ------------------------------- */
function checkScroll() {
    var scroll = 0;
    $("#body").scroll(function (event) {
        scroll = $("#body").scrollTop();
    });

    var body = document.getElementById("body");
    var startPos = -1;
    body.addEventListener("touchstart", function (e) {
        startPos = e.touches[0].clientY;
    }, false);
    body.addEventListener("touchmove", function (e) {
        currentPos = e.touches[0].clientY;
        if (currentPos - startPos > 0 && scroll == 0) {
            body.classList.remove("position-relative");
            body.classList.add("position-absolute");
            if (body.style.top == "0px" || body.style.top == "") {
                document.body.insertBefore(
                    createElement("i", { class: "position-absolute centerHorizontal fad fa-sync-alt", id: "reloadImage" }),
                    body
                );
            }
            body.style.top = "3em";
            var offset = (body.offsetTop + (currentPos - startPos) / 7);
            body.style.top = offset + "px";
            if (offset < 100) {
                var deg = Math.round((offset / 150) * 360);
                document.getElementById("reloadImage").style.transform = "rotate(" + deg + "deg)";
            } else {
                body.classList.add("position-relative");
                body.classList.remove("position-absolute");
                body.style.top = "0px";
                removeElement(document.getElementById("reloadImage"), false);
                changePage(currentPage, {}, false, true);
            }

        } else {
            body.classList.add("position-relative");
            body.classList.remove("position-absolute");
            body.style.top = "0px";
            removeElement(document.getElementById("reloadImage"), false);
        }
    }, false);
    body.addEventListener("touchend", function (e) {
        body.classList.add("position-relative");
        body.classList.remove("position-absolute");
        body.style.top = "0px";
        removeElement(document.getElementById("reloadImage"), false);
    }, false);

}

function view(file, append = false, div = "body", options = {}, callback) {
    var rawFile = new XMLHttpRequest();
    rawFile.open("GET", "view/" + file + "/" + file + ".html", false);
    rawFile.onreadystatechange = function () {
        if (rawFile.readyState === 4) {
            if (rawFile.status === 200 || rawFile.status == 0) {
                var allText = rawFile.responseText;
                if (div == "raw") {
                    if (typeof callback == "function") callback(allText);
                } else {
                    if (append) document.getElementById(div).innerHTML += allText;
                    else document.getElementById(div).innerHTML = allText;
                    if (typeof callback == "function") callback(true);
                    if (typeof func[file] == "function") func[file](options);
                }
            } else {
                if (typeof callback == "function") callback(false);
            }
        } else {
            if (typeof callback == "function") callback(false);
        }
    }.bind(this)
    rawFile.send(null);
}

function changePage(file, options = {}, forward = true, reload = false, isCheckSession = false) {
    //loginPageHeader(file);
    if (currentPage == file && reload == false) return;
    currentPage = file;
    try {
        view(file, false, "raw", options, function (innerHTML) {
            console.log('changePage', file, innerHTML != false);
            if (innerHTML != false) {
                if (isCheckSession) {
                    checkSession();
                }
                animateScreenChange(innerHTML, forward, func[file], options);
                historyData.push({ file: file });
                try {
                    $(".sideMenuContent").removeClass("active");
                    document.getElementById("sideMenuContent" + file).classList.add("active");
                } catch (e) { }
            }
        });
    } catch (e) {
        setTimeout(function () {
            changePage(file);
        }.bind(this), 250);
    }
}

function loginPageHeader(file) {
    if (file.toLowerCase().includes("login")) {
        document.getElementById("headerHamburger").classList.add("d-none");
    } else {
        document.getElementById("headerHamburger").classList.remove("d-none");
    }
}

function animateScreenChange(html, forward = true, callback, options) {
    var body = document.getElementById("body");
    var childPresent = body.children.length;
    if (childPresent) {
        var child = body.children[0];
        child.classList.add("position-absolute");
        child.style.top = "0em";
        child.style.left = "0px";
        child.style.transition = "left 750ms ease-in-out";
        child.style.width = "100vw";
        document.body.appendChild(child);
    }

    var target = $.parseHTML(html)[0];
    target.classList.add("position-absolute");
    target.style.top = "0em";
    target.style.left = (!forward ? "-100vw" : "100vw");
    target.style.width = "100vw";
    target.style.transition = "left 750ms ease-in-out";
    document.body.appendChild(target);
    setTimeout(() => {
        target.style.left = "0px";
        if (childPresent) child.style.left = (!forward ? "100vw" : "-100vw");
        setTimeout(() => {
            target.classList.remove("position-absolute");
            target.classList.add("position-relative");
            target.style.width = "100%";
            target.style.top = "0";
            if (childPresent) removeElement(child, false);
            body.innerHTML = "";
            body.appendChild(target);
            if (typeof callback == "function") callback(options);
        }, 750);
    }, 0);



}


function autoSubstract(e) {
    var elem = $($(e.target).parents(".simplifiedArithmetic")[0]).find("input")[0];
    var step = parseInt(elem.getAttribute("step") || 1);
    var min = parseInt(elem.getAttribute("min") || 0);
    var value = parseInt(elem.value || 0);
    elem.value = (value - step < min ? value : value - step);
}

function autoAdd(e) {
    var elem = $($(e.target).parents(".simplifiedArithmetic")[0]).find("input")[0];
    var step = parseInt(elem.getAttribute("step") || 1);
    var max = parseInt(elem.getAttribute("max") || 100000);
    var value = parseInt(elem.value || 0);
    elem.value = (value + step > max ? value : value + step);
}

function changeBackgroundImage(image) {
    document.getElementById("body").style.backgroundImage = "url('res/img/" + image + "')";
}

function initAutoHeight() {
    setTimeout(() => {
        $(".autoHeight").each((i, elem) => {
            elem.style.height = ((elem.textLength * 16) / elem.offsetWidth) * 60 + "px";
        });
        $(".autoHeight").keyup(e => {
            e.target.style.height = ((e.target.textLength * 16) / e.target.offsetWidth) * 60 + "px";
        });
    }, 0);

}

function initAutoWidth() {
    $("input.autoWidth").each((i, elem) => {
        elem.style.width = elem.value.length + "em";
    })
    $("input.autoWidth").keyup(e => {
        e.target.style.width = e.target.value.length + "em";
    })
}

function removeElement(el, animate = false) {// remove an element from DOM
    try {
        let elem = el;
        if (!animate) {
            elem.parentNode.removeChild(elem);
        } else {
            $(elem).animate({ height: '0' }, 1000, "swing", function () {
                $(elem).animate({ width: '0' }, 1000, "swing", function () {
                    removeElement(elem);
                }.bind(this));
            }.bind(this));
        }
    } catch (e) { }
}

function intToPosition(int) {
    int = int.toString();
    var conversion = { 1: "st", 2: "nd", 3: "rd" };
    var lastNumber = int.substring(int.length - 1);
    if (lastNumber in conversion) return int + "<sup>" + conversion[lastNumber] + "</sup>";
    else return int + "<sup>th</sup>";
}

function createElement(type, attributes, children = [], properties = {}) {
    var elem = document.createElement(type);
    for (var attribute in attributes) {
        elem.setAttribute(attribute, attributes[attribute]);
    }

    for (var i = 0; i < children.length; i++) {
        elem.appendChild(children[i]);
    }

    for (var property in properties) {
        if (property == "innerHTML") {
            elem[property] += properties[property];
        } else {
            elem[property] = properties[property];
        }
    }

    return elem;
}

function generate(length = 5) {
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz012345678g';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

function generateNumber(max, min = 0) {
    return Math.floor(Math.random() * max) + min;
}

function pascalCase(str) {
    if (typeof str == "string") {
        var splitStr = str.toLowerCase().split(' ');
        for (var i = 0; i < splitStr.length; i++) {
            splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
        }
        return splitStr.join(' ');
    } else {
        return "";
    }

}

function showHidePassword(id) {
    var e = document.getElementById(id);
    var icon = e.children[0];
    if (icon.classList.contains("fa-eye")) {
        icon.classList.remove("fa-eye");
        icon.classList.add("fa-eye-slash");
        e.parentNode.children[0].type = "text";
    } else {
        icon.classList.remove("fa-eye-slash");
        icon.classList.add("fa-eye");
        e.parentNode.children[0].type = "password";
    }
}

function displayUITable(elem, arr, conversion = {},
    inwardHooks = {},
    inwardOptions = { clear: true, image: true, dataTable: { state: true, params: null }, tableClass: "" },
    inwardHeadException = [], inwardBodyException = ["null"], invisibleHead = []) {// display table on DOM based on array
    //@author Nurmaan Ramoly
    //@params arr -> [{head:content}]
    //[{username:aaa, password: bbb},{username: ccc, password, uuu}]

    //set inner variables
    var hooks = {
        beforeBody: inwardHooks.beforeBody || null,
        afterBody: inwardHooks.afterBody || null,
        beforeHead: inwardHooks.beforeHead || null,
        afterHead: inwardHooks.afterHead || null,
        afterRow: inwardHooks.afterRow || null
    };
    var options = [];
    options['clear'] = typeof inwardOptions['clear'] == "undefined" ? true : inwardOptions['clear'];
    options['image'] = typeof inwardOptions['image'] == "undefined" ? true : inwardOptions['image'];
    options['dataTable'] = typeof inwardOptions['dataTable'] == "undefined" ? { state: true, fn: null } : inwardOptions['dataTable'];
    options['tableClass'] = typeof inwardOptions['tableClass'] == "undefined" ? "" : inwardOptions['tableClass'];
    options['tableContainerClass'] = typeof inwardOptions['tableContainerClass'] == "undefined" ? "" : inwardOptions['tableContainerClass'];
    options['tableBodyClass'] = typeof inwardOptions['tableBodyClass'] == "undefined" ? "" : inwardOptions['tableBodyClass'];

    var headException = ["vehicleID", "requestID"];
    for (var i = 0; i < inwardHeadException.length; i++) {
        headException.push(inwardHeadException[i]);
    }
    var bodyException = [];
    for (var i = 0; i < inwardBodyException.length; i++) {
        bodyException.push(inwardBodyException[i]);
    }

    if (options.clear) elem.innerHTML = "";
    if (arr.length <= 0) {
        elem.appendChild(createElement("h3", { class: "text-center p-3" }, [], { innerHTML: "No Data" }));
        return -1;
    }
    var id = generate();
    var table =
        createElement("table", { class: "table table-hover w-100 " + inwardOptions['tableClass'], id: "table-" + id }, [
            createElement("thead", { id: "tableHead-" + id, class: "thead-light" }),
            createElement("tbody", { id: "tableBody-" + id })
        ]);
    elem.appendChild(
        createElement("div", { class: inwardOptions['tableContainerClass'] }, [table])
    );


    for (var i = 0; i < arr.length; i++) {
        var head = createElement("tr");
        var body = createElement("tr");
        for (var key in arr[i]) {
            var datum = arr[i][key];
            if (headException.indexOf(key) != -1 || (bodyException.indexOf(datum) != -1)) { } else {
                if (datum == null && !(options.image && key == "image")) datum = "None";
                var invisible = invisibleHead.indexOf(key) > -1;
                var beforeBodyFunction;
                if (typeof hooks.beforeBody == "function") {
                    beforeBodyFunction = hooks.beforeBody(body, i, key, arr); // {node,raw,stopProcessing}
                    if (typeof beforeBodyFunction != "undefined" && beforeBodyFunction.node != false) {
                        var node;
                        if (!beforeBodyFunction.raw) {
                            node = createElement("td", { class: invisible ? "d-none" : "" }, [beforeBodyFunction.node]);
                            body.appendChild(node);
                        } else {
                            nodes = beforeBodyFunction.node;
                            for (var node of nodes) {
                                body.appendChild(node);
                            }
                        }
                    };
                }

                if (typeof beforeBodyFunction == "undefined" || !beforeBodyFunction.stopProcessing) {
                    if (options.image && key == "image") {
                        body.appendChild(createElement("td", { class: invisible ? "d-none" : "" }, [
                            createElement("img", { src: (datum != null ? baseImage + datum + "_small" : baseResourceImage + "noImage"), style: "max-height: 10vh;" })
                        ]));
                    } else {
                        body.appendChild(createElement("td", { class: invisible ? "d-none" : "" }, [], { innerHTML: datum }));
                    }
                }


                if (i == 0) {
                    var beforeHeadFunction;
                    if (typeof hooks.beforeHead == "function") {
                        beforeHeadFunction = hooks.beforeHead(head, key, arr);
                        if (typeof beforeHeadFunction != "undefined" && beforeHeadFunction.node != false) {
                            var node;
                            if (!beforeHeadFunction.raw) {
                                node = createElement("th", { class: invisible ? "d-none" : "" }, [beforeHeadFunction.node])
                            } else {
                                node = beforeHeadFunction.node;
                            }
                            head.appendChild(node);
                        };
                    }
                    if (typeof beforeHeadFunction == "undefined" || !beforeHeadFunction.stopProcessing) {
                        head.appendChild(createElement("th", { class: invisible ? "d-none" : "" }, [], { innerHTML: (key in conversion ? conversion[key] : pascalCase(key)) }));
                    }

                }
            }
        }

        if (i == 0) {
            if (typeof hooks.afterHead == "function") head.appendChild(hooks.afterHead(head, i, arr));
            document.getElementById("tableHead-" + id).appendChild(head);
        }

        if (typeof hooks.afterBody == "function") body.appendChild(createElement("td", {}, [hooks.afterBody(body, i, arr)]));
        document.getElementById("tableBody-" + id).appendChild(body);
        if (typeof hooks.afterRow == "function") document.getElementById("tableBody-" + id).appendChild(hooks.afterRow(i, arr));

    }

    if (options.dataTable.state) {
        $("#table-" + id).DataTable(options.dataTable.params);
    }

    return id;
}

function getRandomInt(max, min = 0) {
    return Math.floor(Math.random() * Math.floor(max - min)) + min;
}

function replaceElem(targetElem, replaceWith, input = 1, keepHTML = false) {
    var attributes = concatHashToString(targetElem.attributes);
    var replacingStartTag = '<' + replaceWith + attributes + '>';
    var replacingEndTag = '</' + replaceWith + '>';
    var elem = $.parseHTML(replacingStartTag + (keepHTML ? $(targetElem).html() : "") + replacingEndTag)[0];
    var value = (input == 2 ? targetElem.value : targetElem.innerHTML);
    $(targetElem).replaceWith(elem);
    if (input == 1) elem.value = value;
    else if (input == 2) elem.innerHTML = value;
    return elem;
}

function concatHashToString(hash) {
    var emptyStr = '';
    $.each(hash, function (index) {
        emptyStr += ' ' + hash[index].name + '="' + hash[index].value + '"';
    });
    return emptyStr;
}

function NewGuid() {
    var sGuid = "";
    for (var i = 0; i < 32; i++) {
        sGuid += Math.floor(Math.random() * 0xF).toString(0xF);
    }
    return sGuid;
}
