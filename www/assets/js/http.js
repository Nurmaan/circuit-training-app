function Http(url, success, error) {
    this.url = baseURL + url;
    this.success = success;
    this.error = error;
    this.count = 0;
    function post(params, loader = false, async = true, forceOnline = false, url = this.url, success = this.success, error = this.error, count = this.count) {

        console.log("Posting to ", url, count, window.navigator.onLine);
        if (window.navigator.onLine) {

            //if (loader) displayLoader();

            if (count < 3) {
                $.ajax({
                    type: "post",
                    url: url,
                    data: params,
                    dataType: "json",
                    timeout: 10000,
                    beforesend: function () {
                        if (loader) displayLoader();
                    },
                    headers: { Usertoken: localStorage.getItem("userToken") || "" },
                    async: async,
                    success: function (data) {
                        if (url.includes("login")) parseLoginResult(data.data);
                        console.log('success', data);
                        count = 0;
                        if (data['success']) {
                            if (typeof success == "function") success(data['data']);
                        } else {
                            if (data['error'].toLowerCase() == "session expired") changePage("login");
                            toastr.error(data['error']);
                            console.log(data['error']);
                            if (typeof error == "function") error(data['error']);
                        }
                        //hideLoader();
                    },
                    error: function (data) {
                        //hideLoader();
                        console.log('error', data.responseText);
                        toastr.error('error', JSON.stringify(data));
                        count++;
                        post(params, url, success, error, count);
                    }
                });
            } else {
                toastr.error("Oops! It looks like there has been an error on our side...");
            }

        } else {
            if (!forceOnline) {
                toastr.warning("Please check your internet connection", "No Connection");
                if (typeof success == "function") success(-1)
            } else {
                navigator.notification.confirm("You Seem to be offline. Please Check your Connection and Try Again", function (index) {
                    if (index == 1) {
                        post(params, url, success, error, count);
                    } else {
                        navigator.app.exitApp();
                    }
                }, "No Internet", ["Try Again"]);
            }
        }

    }

    this.post = post;
}

function get(url, options, success, error) {

}

function uploadImage(imageURI, send, onprogress, success, error) {
    var options = new FileUploadOptions();
    options.fileKey = "file";
    options.fileName = imageURI.substr(imageURI.lastIndexOf('/') + 1);
    options.mimeType = "image/png";

    options.params = { data: send };
    options.chunkedMode = false;

    showProgressLoader();

    var ft = new FileTransfer();
    ft.onprogress = function (progressEvent) {
        if (progressEvent.lengthComputable) {
            //console.log(progressEvent.loaded / progressEvent.total);
            document.getElementById("progressLoaderValue").innerHTML = Math.round((progressEvent.loaded / progressEvent.total) * 100) + "%";
            if (typeof onprogress == "function") onprogress(progressEvent.loaded / progressEvent.total, progressEvent);
        }
    };
    ft.upload(imageURI, baseURL + "food.php", function (result) {
        hideProgressLoader();
        toastr.success("Image Uploaded!");
        if (typeof success == "function") success(result);
        console.log(result.responseText);
    }, function (err) {

        hideProgressLoader();
        toastr.error("Sorry an error occured while uploading. Check your internet connection and try again");
        if (typeof error == "function") error(err);
        console.log('error : ', err);
    }, options);
}

function parseLoginResult(data) {
    localStorage.setItem("userToken", data.userToken);
    localStorage.setItem("userType", data.userType);
    localStorage.setItem("username", data.fullName);
}