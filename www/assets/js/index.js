var func = {};
var currentLocation = -1;
var baseURL = "http://nurmaan.mauriapp.info/workout/api/";
var IMAGE_URL = "http://scootit.nurmaan.mauriapp.info/images/";
var db;
var allData = { dish: [], shop: [] };
var historyData = [];
var user = {};
var addDishPreferenceCount = 0;
document.addEventListener('deviceready', deviceReady, false);

function deviceReady() {
    document.addEventListener("backbutton", backPress, true);
    init();
}

function setAttributes(elem, attribute) {
    for (var key in attribute) {
        elem.setAttribute(key, attribute[key]);
    }
}

function router() {
    $('.router').unbind().click(function () {
        changePage(this.getAttribute('link'));
        //hideMenu();
    });
}


function autoHeight() {
    $(".autoHeight").unbind().keyup(function () {
        var val = this.value.length;
        var width = document.body.offsetWidth;
        this.style.height = (((val * 14) / width) * 15) + "px";
    });
}
function backPress(e) {
    //console.log("historyData", JSON.stringify(historyData));
    e.preventDefault();
    if (historyData.length == 0 || typeof historyData[historyData.length - 2] == "undefined") {
        navigator.notification.confirm("Are You Sure You Want To Exit?", function (index) {
            switch (index) {
                case 1:
                    navigator.app.exitApp();
                    break;
            }
        }, "Exit", ["Yeah😕", "Not At All😲"]);
    } else {
        view(historyData[historyData.length - 2]['file'], historyData[historyData.length - 2]['div'], false);
        historyData.splice(-1, 1);
    }
}

function loadLocation(url) {
    window.location.href = url;
}

function setHeights() {
    document.getElementById("body").style.height = getBodyHeight() + "px";
    document.getElementById("bodyContainer").style.height = getBodyHeight() + "px";
}

function getBodyHeight() {
    return ($(window).height() - 40);
}

function removeElement(elem, animate = false) {
    //console.log(elem, animate);
    if (!animate) {
        elem.parentNode.removeChild(elem);
    } else {
        $(elem).animate({ height: '0' }, 1000, "swing", function () {
            $(this).animate({ width: '0' }, 1000, "swing", function () {
                //console.log(this);
                removeElement(this);
            }.bind(elem));
        }.bind(elem));
    }
}

function removeIndirectElement(id, animate = false) {
    var elem = document.getElementById(id);
    //console.log("removeIndirectElement", id, elem, animate);
    removeElement(elem, animate);
}

function createElement(type, attributes, children = [], properties = {}) {
    var elem = document.createElement(type);
    for (var attribute in attributes) {
        elem.setAttribute(attribute, attributes[attribute]);
    }
    for (var property in properties) {
        elem[property] = properties[property];
    }

    for (var i = 0; i < children.length; i++) {
        elem.appendChild(children[i]);
    }

    return elem;
}

function generate(length = 5) {
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

function getParentByClassName(elem, cls) {
    for (var i = 0; i < 20; i++) {
        if (elem.classList.contains(cls)) return elem;
        else elem = elem.parentNode;
    }

    return false;
}

function confirm(title = "ShopShare", message, success, error, buttons = ["Confirm", "Cancel"]) {
    $('#confirm').modal('show');
    $('#confirm')[0].style.zIndex = "10001";

    //send to foreground
    var modals = document.getElementsByClassName("modal-backdrop fade show");
    modals[modals.length - 1].style.zIndex = "10000";

    $('#confirmAccept')[0].innerHTML = buttons[0];
    if (buttons.length > 1) {
        $('#confirmClose')[0].innerHTML = buttons[1];
        $('#confirmClose').removeClass("d-none");
    } else {
        $('#confirmClose').addClass("d-none");
    }
    $('#confirmBody')[0].innerHTML = message;
    $('#confirmTitle')[0].innerHTML = title;

    $('#confirmClose').unbind().click(function () {
        if (typeof error == "function") error();
    });
    $('#confirmAccept').unbind().click(function () {
        if (typeof success == "function") success();
    });
}